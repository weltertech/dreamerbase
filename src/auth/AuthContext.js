import React from 'react'
import firebase from 'firebase/app'
import 'firebase/auth'
import 'firebase/database'
import PropTypes from 'prop-types'

import { GA } from '../constants'
import GAEvent from '../modules/analytics-event-helper'

const AuthContext = React.createContext()

const providers = {
  FACEBOOK: 'FACEBOOK',
  GOOGLE: 'GOOGLE',
  TWITTER: 'TWITTER',
  GITHUB: 'GITHUB'
}

class AuthProvider extends React.Component {
  constructor () {
    super()

    this.handleFacebookSuccess = this.handleFacebookSuccess.bind(this)
    this.handleFacebookError = this.handleFacebookError.bind(this)
    this.refreshProfile = this.refreshProfile.bind(this)
    this.completeLoading = this.completeLoading.bind(this)

    this.login = this.login.bind(this)
    this.logout = this.logout.bind(this)

    this.state = {
      anonymous: undefined,
      user: undefined,
      userName: undefined,
      userLoading: true
    }
  }

  refreshProfile () {
    if (this.state.user) {
      firebase.database().ref(`profiles/${this.state.user.uid}/userName`).once('value', (profileSnapshot) => {
        this.setState({
          userName: profileSnapshot.val()
        })
        this.completeLoading()
      })
    }
  }

  completeLoading () {
    this.setState({
      userLoading: false
    })
  }

  componentDidMount () {
    firebase.auth().onAuthStateChanged(user => {
      if (user) {
        if (user.isAnonymous === true) {
          this.setState({ anonymous: user, user })
          this.completeLoading()
        } else {
          this.setState({ anonymous: undefined, user })
          this.completeLoading()
          // attempt to get userName if non-anonymous user
          firebase.database().ref(`profiles/${user.uid}/userName`).once('value', (profileSnapshot) => {
            this.setState({
              userName: profileSnapshot.val()
            })
            this.completeLoading()
          })
        }
      } else {
        // this.setState({ user: null, userLoading: false })
        firebase.auth().signInAnonymously().catch((err) => {
          console.log('anonymous login error', err)
        })
      }
    })
  }

  handleFacebookSuccess (result) {
    // The signed-in user info.
    var user = result.user
    this.setState({ user })
  }

  handleFacebookError (error) {
    console.log(JSON.stringify(error, null, 2))
  }

  login (providerKey = providers.FACEBOOK) {
    GAEvent({
      category: GA.CATEGORY.LOGIN,
      action: GA.ACTION.LOGIN_FACEBOOK,
      label: 'na'
    })

    let provider

    switch (providerKey) {
      case providers.FACEBOOK:
        provider = new firebase.auth.FacebookAuthProvider()
        break
      case providers.GOOGLE:
        provider = new firebase.auth.GoogleAuthProvider()
        break
      case providers.TWITTER:
        provider = new firebase.auth.TwitterAuthProvider()
        break
      case providers.GITHUB:
        provider = new firebase.auth.GithubAuthProvider()
        break
      default:
    }

    firebase.auth().signInWithPopup(provider)
      .then(result => this.handleSocialSuccess(result))
      .catch(error => this.handleSocialError(error))
  }

  logout () {
    GAEvent({
      category: GA.CATEGORY.LOGIN,
      action: GA.ACTION.LOGOUT,
      label: 'na'
    })

    firebase.auth().signOut()
      .then(() => {
        this.setState({
          user: null
        })
      })
  }

  render () {
    // console.log('auth context re-render state', this.state)
    return (
      <AuthContext.Provider
        value={{
          anonymous: this.state.anonymous,
          user: this.state.user,
          userName: this.state.userName,
          userLoading: this.state.userLoading,
          login: this.login,
          logout: this.logout,
          refreshProfile: this.refreshProfile
        }}
      >
        {this.props.children}
      </AuthContext.Provider>
    )
  }
}
const AuthConsumer = AuthContext.Consumer

AuthProvider.propTypes = {
  children: PropTypes.element
}

export { AuthProvider, AuthConsumer, providers }
