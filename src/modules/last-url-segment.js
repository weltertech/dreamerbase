export default (url = '') => {
  try {
    return url.split('/').pop()
  } catch (e) {
    return ''
  }
}
