export default (x = 0) => {
  try {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',')
  } catch (e) {
    return 0
  }
}
