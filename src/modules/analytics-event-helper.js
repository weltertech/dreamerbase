
import firebase from 'firebase/app'
import 'firebase/auth'
import ReactGA from 'react-ga'

// Anonymous user set Google Analytics VALUE to 0
const getDefaultValue = () => {
  return firebase.auth().currentUser.isAnonymous === true ? 0 : 1
}

const enabled = process.env.REACT_APP_GOOGLE_ANALYTICS_ENABLED || false

export default ({ action, category, label, value = getDefaultValue() }) => {
  if (enabled) {
    ReactGA.event({
      category,
      action,
      label,
      value
    })
  }
}
