
import 'firebase/auth'
import ReactGA from 'react-ga'

const enabled = process.env.REACT_APP_GOOGLE_ANALYTICS_ENABLED || false

export default (page) => {
  if (enabled) {
    ReactGA.pageview(page)
  }
}
