import defaultUserName from './default-user-name'

test('works when email', () => {
  const authUser = {
    email: 'brian@habitualdreamers.com'
  }
  const expected = 'brian'
  expect(defaultUserName(authUser)).toEqual(expected)
})

test('works when display name with space', () => {
  const authUser = {
    displayName: 'Brian Welter'
  }
  const result = defaultUserName(authUser)

  expect(result.startsWith('Brian')).toEqual(true)
})

test('works when display name with NO space', () => {
  const authUser = {
    displayName: 'BrianWelter'
  }
  const result = defaultUserName(authUser)

  expect(result.startsWith('BrianWelter')).toEqual(true)
})

test('works when no email or display name', () => {
  const authUser = {
  }
  const result = defaultUserName(authUser)

  expect(result.startsWith('User')).toEqual(true)
})
