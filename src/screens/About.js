import React from 'react'
import styled from 'styled-components'
import Container from '../components/Container'
import StandardText from '../components/Text'
import MetaTags from '../components/MetaTags'
import GAPage from '../modules/analytics-pageview-helper'

const Text = styled(StandardText)`
  font-size: 1.3em;
`

export default () => {
  GAPage('/about')

  return (
    <Container>
      <MetaTags title='About Page' />
      <h1>About</h1>
      <Text>This is what its all about.</Text>
    </Container>
  )
}
