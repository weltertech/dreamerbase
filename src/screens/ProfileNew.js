import React, { Component } from 'react'
import firebase from 'firebase/app'
import 'firebase/auth'
import 'firebase/database'
import PropTypes from 'prop-types'
import _debounce from 'lodash.debounce'

// components
import Button from '../components/Button'
import InputContainer from '../components/Input'
import Container, { SingleCenteredColumn } from '../components/Container'
import Loading from '../components/Loading'

// modules
import getUserName from '../modules/default-user-name'

// auth
import { AuthConsumer } from '../auth/AuthContext'

import { GA } from '../constants'

import GAPage from '../modules/analytics-pageview-helper'
import GAEvent from '../modules/analytics-event-helper'

import { Textbox } from 'react-inputs-validation'
import 'react-inputs-validation/lib/react-inputs-validation.min.css'

import { tertiary } from '../variables'
import { clearTimeout } from 'timers'

class ProfileNew extends Component {
  constructor (props) {
    super(props)
    this.state = {
      userName: getUserName(firebase.auth().currentUser),
      nameAvailable: true,
      nameInvalid: true,
      saved: false,
      checkedName: false
    }
    this.updateProfile = this.updateProfile.bind(this)
    this.saveProfile = this.saveProfile.bind(this)
    this.fieldInvalid = this.fieldInvalid.bind(this)
  }

  componentDidMount () {
    this.nameLookup(this.state.userName)
    this.delayPageView = setTimeout(() => {
      GAPage('/newProfile')
    }, 750)
  }

  componentWillUnmount () {
    try {
      clearTimeout(this.delayPageView)
    } catch (e) {
    }
    try {
      clearTimeout(this.delayedNameCheck)
    } catch (e) {
    }
  }

  componentWillMount () {
    this.nameLookup = (userName) => {
      GAEvent({
        category: GA.CATEGORY.PROFILE,
        action: GA.ACTION.VERFIY_USERNAME,
        label: userName
      })

      this.lookupNameRef = firebase.database().ref('profiles')
      this.lookupNameRef.orderByChild('userNameLower').equalTo(userName.toLowerCase()).on('value', snapshot => {
        const nameAvailable = snapshot.val() === null
        this.setState({
          nameAvailable,
          checkedName: true
        })
      })
    }

    this.delayedNameCheck = _debounce(function (userName) {
      this.nameLookup(userName)
    }, 500)
  }

  updateProfile (e) {
    this.setState({
      [e.target.name]: e.target.value,
      nameAvailable: false,
      checkedName: false
    })

    if (e.target.name === 'userName') {
      this.delayedNameCheck(e.target.value)
    }
  }

  saveProfile (postSaveFunc) {
    const saveRef = firebase.database().ref(`/profiles/${firebase.auth().currentUser.uid}`)
    saveRef.set({
      userName: this.state.userName,
      userNameLower: this.state.userName.toLowerCase()
    }).then(success => {
      GAEvent({
        category: GA.CATEGORY.PROFILE,
        action: GA.ACTION.CREATED,
        label: firebase.auth().currentUser.uid
      })

      this.setState({
        saved: true
      })
      _debounce(function (userName) {
        postSaveFunc()
      }, 500)()
    }).catch(failed => {

    })
  }

  fieldInvalid (isInvalid) {
    this.setState({
      nameInvalid: isInvalid
    })
  }

  render () {
    const loader = {
      height: '50px',
      width: '50px'
    }

    const textInput = {
      fontSize: '1.2em',
      padding: '0.4em',
      background: 'white',
      border: `1px solid ${tertiary}`
    }

    return (
      <AuthConsumer>
        {({ refreshProfile }) => (
          <Container className='App'>
            <SingleCenteredColumn>
              <h2>Welcome to Dreamerbase...pick a user name and we can get started.</h2>

              <form onChange={this.updateProfile}>
                <InputContainer>
                  <label htmlFor='title'>User Name (3-50 characters)</label>
                  <Textbox
                    name='userName'
                    value={this.state.userName}
                    customStyleInput={textInput}
                    validationOption={{
                      min: 3,
                      max: 49,
                      name: `UserName`,
                      required: true,
                      reg: /^[a-z0-9]+$/i,
                      regMsg: 'Username must only contain letters and numbers'
                    }}
                    onBlur={e => console.log(e)}
                    onChange={e => console.log(e)}
                    validationCallback={this.fieldInvalid}
                  />
                  { !this.state.nameAvailable && this.state.checkedName && !this.state.saved ? <span>User name already taken!</span> : null }
                  { !this.state.nameAvailable && !this.state.checkedName ? <div className={loader}><Loading /></div> : null }
                </InputContainer>
                <Button
                  primary
                  type='button'
                  onClick={e => this.saveProfile(refreshProfile)}
                  disabled={
                    !this.state.checkedName ||
                    !this.state.nameAvailable ||
                    this.state.saved
                  }
                >Save</Button>
              </form>
            </SingleCenteredColumn>
          </Container>
        )}
      </AuthConsumer>
    )
  }
}

ProfileNew.propTypes = {
  history: PropTypes.object,
  match: PropTypes.object,
  refreshProfile: PropTypes.func
}

export default ProfileNew
