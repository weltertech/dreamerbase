import React from 'react'
import styled from 'styled-components'
import Container from '../components/Container'
import StandardText from '../components/Text'
import MetaTags from '../components/MetaTags'
import GAPage from '../modules/analytics-pageview-helper'

const Text = styled(StandardText)`
  font-size: 1.3em;
`

export default () => {
  GAPage('/home')

  return (
    <Container>
      <MetaTags title='Home Page' />
      <h1>Home</h1>
      <Text>Welcome to the Dreamerbase: a React/Firebase starter.</Text>
    </Container>
  )
}
