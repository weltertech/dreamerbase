import React, { Component } from 'react'
import firebase from 'firebase/app'
import 'firebase/auth'
import PropTypes from 'prop-types'
import Container, { SingleCenteredColumn } from '../components/Container'
import Button from '../components/Button'
import InputContainer from '../components/Input'
import { AuthConsumer, providers } from '../auth/AuthContext'
import MetaTags from '../components/MetaTags'
import InfoBox from '../components/InfoBox'

import { FacebookLoginButton, GoogleLoginButton, GithubLoginButton, TwitterLoginButton } from 'react-social-login-buttons'

import { GA } from '../constants'

import GAEvent from '../modules/analytics-event-helper'
import GAPage from '../modules/analytics-pageview-helper'

const facebookEnabled = process.env.REACT_APP_AUTH_FACEBOOK_ENABLED
const twitterEnabled = process.env.REACT_APP_AUTH_TWITTER_ENABLED
const githubEnabled = process.env.REACT_APP_AUTH_GITHUB_ENABLED
const googleEnabled = process.env.REACT_APP_AUTH_GG_ENABLED

const getBaseUrl = (url = window.location) => {
  if (url.port) {
    return `${url.protocol}//${url.hostname}:${url.port}`
  }
  return `${url.protocol}//${url.hostname}`
}

const parseQuery = (qs) => {
  const values = {}
  try {
    const parts = qs.replace('?', '').split('&')
    parts.forEach((part) => {
      const keyValue = part.split('=')
      values[keyValue[0]] = keyValue[1]
    })
  } catch (e) {

  }
  return values
}

class EmailLogin extends Component {
  constructor (props) {
    super(props)
    this.state = {
      emailSent: false,
      email: '',
      redirect: window.previousLocation || '/',
      deepLink: decodeURIComponent(parseQuery(window.location.search).redirect)
    }
    // console.log('winprev', window.previousLocation)
    this.emailChange = this.emailChange.bind(this)
    this.sendLoginLink = this.sendLoginLink.bind(this)
  }

  componentDidMount () {
    GAPage('/login')
  }

  componentWillMount () {
    const { state, props } = this
    // Confirm the link is a sign-in with email link.
    if (firebase.auth().isSignInWithEmailLink(window.location.href)) {
      // Additional state parameters can also be passed via URL.
      // This can be used to continue the user's intended action before triggering
      // the sign-in operation.
      // Get the email if available. This should be available if the user completes
      // the flow on the same device where they started it.
      var email = window.localStorage.getItem('emailForSignIn')
      if (!email) {
        // User opened the link on a different device. To prevent session fixation
        // attacks, ask the user to provide the associated email again. For example:
        GAEvent({
          category: GA.CATEGORY.LOGIN,
          action: GA.ACTION.LOGIN_PROMPT_EMAIL,
          label: 'na'
        })

        email = window.prompt('Please provide your email for confirmation')
      }
      // The client SDK will parse the code from the link for you.
      firebase.auth().signInWithEmailLink(email, window.location.href)
        .then(function (result) {
          // Clear email from storage.
          window.localStorage.removeItem('emailForSignIn')

          GAEvent({
            category: GA.CATEGORY.LOGIN,
            action: GA.ACTION.LOGIN_COMPLETE_EMAIL,
            label: 'na'
          })

          if (state.deepLink) {
            props.history.push(state.deepLink)
          }
          // You can access the new user via result.user
          // Additional user info profile not available via:
          // result.additionalUserInfo.profile == null
          // You can check if the user is new or existing:
          // result.additionalUserInfo.isNewUser
        })
        .catch(function (error) {
          GAEvent({
            category: GA.CATEGORY.LOGIN,
            action: GA.ACTION.LOGIN_FAILED_EMAIL,
            label: 'na'
          })
          console.log(error)
          // Some error occurred, you can inspect the code: error.code
          // Common errors could be invalid email and invalid or expired OTPs.
        })
    }
  }

  emailChange (e) {
    this.setState({
      email: e.target.value
    })
    window.localStorage.setItem('emailForSignIn', e.target.value)
  }

  sendLoginLink () {
    this.setState({
      emailSent: true
    })

    const actionCodeSettings = {
      // URL you want to redirect back to. The domain (www.example.com) for this
      // URL must be whitelisted in the Firebase Console.
      url: `${getBaseUrl()}/login?redirect=${this.state.redirect}`,
      // This must be true.
      handleCodeInApp: true
    }

    GAEvent({
      category: GA.CATEGORY.LOGIN,
      action: GA.ACTION.LOGIN_INIT_EMAIL,
      label: 'na'
    })

    firebase.auth().sendSignInLinkToEmail(this.state.email, actionCodeSettings)
  }

  render () {
    const historyref = this.props.history
    return (
      <React.Fragment>
        <MetaTags title='Login' />
        <AuthConsumer>
          {({ anonymous, user, login }) => {
            if (user && !anonymous) {
              setTimeout(() => {
                historyref.push(this.state.redirect)
              }, 1000)
            }
            return (
              <Container className='login-container'>
                <InfoBox>Login Screen</InfoBox>
                <SingleCenteredColumn>
                  { (this.state.emailSent && (!user || anonymous)) ? <h1>Login link sent, check your email</h1> : null }
                  { (!this.state.emailSent && (!user || anonymous))
                    ? <React.Fragment>
                      <div>
                        <InputContainer key='emailInputContainer'>
                          <label htmlFor='email'>Email</label>
                          <input type='email' name='email' value={this.state.email} onChange={this.emailChange} />
                        </InputContainer>
                        <Button primary type='button' onClick={this.sendLoginLink}>Send Login Link</Button>
                      </div>
                      <div style={{ marginBottom: '2em', marginTop: '1em' }}>-- or --</div>
                      { facebookEnabled && <div>
                        <FacebookLoginButton onClick={e => login(providers.FACEBOOK)} />
                      </div> }
                      { googleEnabled && <div>
                        <GoogleLoginButton onClick={e => login(providers.GOOGLE)} />
                      </div> }
                      { githubEnabled && <div>
                        <GithubLoginButton onClick={e => login(providers.GITHUB)} />
                      </div> }
                      { twitterEnabled && <div>
                        <TwitterLoginButton onClick={e => login(providers.TWITTER)} />
                      </div> }
                    </React.Fragment> : null }
                  { user && !anonymous && <h1>Login successful! You will be redirected.</h1> }
                </SingleCenteredColumn>
              </Container>
            )
          }}
        </AuthConsumer>
      </React.Fragment>
    )
  }
}

EmailLogin.propTypes = {
  history: PropTypes.object,
  match: PropTypes.object
}

export default EmailLogin
