import React from 'react'
import ReactDOM from 'react-dom'
import dotenv from 'dotenv'
import firebase from 'firebase/app'
import ReactGA from 'react-ga'
import { BrowserRouter, Route, Switch } from 'react-router-dom'

// screens
import About from './screens/About'
import Login from './screens/Login'
import ProfileNew from './screens/ProfileNew'
import Privacy from './screens/Privacy'
import Terms from './screens/Terms'
import Home from  './screens/Home'

// components
import Header from './components/Header'
import Footer from './components/Footer'
import Loading from './components/Loading'

// auth
import { AuthProvider, AuthConsumer } from './auth/AuthContext'

dotenv.config()

const config = {
  apiKey: process.env.REACT_APP_FIREBASE_API_KEY,
  authDomain: process.env.REACT_APP_FIREBASE_AUTH_DOMAIN,
  databaseURL: process.env.REACT_APP_FIREBASE_DATABASE_URL,
  projectId: process.env.REACT_APP_FIREBASE_PROJECT_ID,
  storageBucket: process.env.REACT_APP_FIREBASE_STORAGE_BUCKET,
  messagingSenderId: process.env.REACT_APP_MESSAGE_SENDER_ID
}

const userNameRequired = process.env.REACT_APP_USERNAME_REQURED || false

try {
  firebase.initializeApp(config)
} catch (error) {
  // already initialized
  console.error(error)
}

if (process.env.REACT_APP_ANALYTICS_ENABLED) {
  ReactGA.initialize(process.env.REACT_APP_ANALYTICS_ID)
}

ReactDOM.render((
  <BrowserRouter>
    <AuthProvider>
      <AuthConsumer>
        {({ anonymous, user, userName, userLoading }) => (
          (!userLoading)
            ? <React.Fragment>
              <Header />
              <Switch>
                { (user && !anonymous) && !userName && userNameRequired && <Route path='/' component={ProfileNew} /> }
                <Route exact path='/' component={Home} />
                <Route exact path='/about' component={About} />
                <Route exact path='/privacy' component={Privacy} />
                <Route exact path='/terms' component={Terms} />
                <Route exact path='/login' component={Login} />
              </Switch>
              <Footer />
            </React.Fragment>
            : <Loading />
        )}
      </AuthConsumer>

    </AuthProvider>
  </BrowserRouter>
), document.getElementById('root'))

// registerServiceWorker()
