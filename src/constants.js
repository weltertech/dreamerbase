export const GA = {
  CATEGORY: {
    PROFILE: 'Profile',
    LOGIN: 'Login'
  },
  ACTION: {
    LOGIN_PROMPT_EMAIL: 'LoginPromptEmail',
    LOGIN_COMPLETE_EMAIL: 'LoginCompleteEmail',
    LOGIN_INIT_EMAIL: 'LoginInitEmail',
    LOGIN_FACEBOOK: 'LoginInitFacebook',
    LOGIN_FAILED_EMAIL: 'LoginFailedEmail',
    LOGOUT: 'Logout',
    VERFIY_USERNAME: 'VerifyUserName',
    CREATED: 'Created'
  }
}
