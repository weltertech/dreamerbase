import styled from 'styled-components'

export default styled.p`
  margin-bottom: 1em;
  line-height: 1.2em;
`
