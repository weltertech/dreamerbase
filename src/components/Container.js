import styled from 'styled-components'

export const SingleCenteredColumn = styled.div`
  max-width: 500px;
  margin: 0;
`

export default styled.div`
  padding: 0.8em;
`
