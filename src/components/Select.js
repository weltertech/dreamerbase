import styled from 'styled-components'

export default styled.select`
  border: 1px solid #dadada;
  background: transparent;
  font-size: 1.1em;
`
