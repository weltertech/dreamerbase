import React from 'react'
import Helmet from 'react-helmet'
import PropTypes from 'prop-types'

const defaultTitle = 'Sortada.com'
const defaultDescription = 'Sort it all out at Sortada.com'

const MetaTags = ({ title = defaultTitle, url, description = defaultDescription }) => (
  <Helmet>
    <title>{title}</title>
    <meta property='og:title' content={title} />
    <meta property='og:description' content={description} />
    <meta property='description' content={description} />
    { url && <meta property='og:url' content={url} /> }
  </Helmet>
)

MetaTags.propTypes = {
  title: PropTypes.string,
  url: PropTypes.string,
  description: PropTypes.string
}

export default MetaTags
