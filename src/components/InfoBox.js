import styled from 'styled-components'
import { info } from '../variables'

export default styled.div`
  background: ${info};
  padding: 1em;
  margin: 0 0 2em 0;
`
