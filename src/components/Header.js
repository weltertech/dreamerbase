import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import styled from 'styled-components'
import { slide as Menu } from 'react-burger-menu'
import { AuthConsumer } from '../auth/AuthContext'
import Logo from '../assets/anon.png'
import NavBarTitle from '../components/NavBarTitle'
import { penny, tertiary } from '../variables'

var styles = {
  bmBurgerButton: {
    position: 'absolute',
    width: '26px',
    height: '28px',
    right: '26px',
    top: '19px'
  },
  bmBurgerBars: {
    background: tertiary
  },
  bmCrossButton: {
    height: '24px',
    width: '24px',
    background: 'transparent'
  },
  bmCross: {
    background: penny
  },
  bmMenu: {
    background: tertiary,
    padding: '2.5em 1.5em 0',
    fontSize: '1.15em'
  },
  bmMorphShape: {
    fill: penny
  },
  bmItemList: {
    color: '#b8b7ad',
    padding: '0.8em'
  },
  bmItem: {
    display: 'block',
    color: 'white',
    textDecoration: 'none',
    marginBottom: '8px'
  },
  bmOverlay: {
    background: 'rgba(0, 0, 0, 0.3)',
    top: 0,
    left: 0
  },
  bmMenuWrap: {
    top: 0,
    height: '100vh'
  }
}

const StyledMenu = styled(Menu)`
  hr {
    border: 0;
    border-top: 1px solid #9FD6A3;
    display: block;
    height: 1px;
  }
`

const avatarStyle = {
  maxHeight: '48px',
  borderRadius: '50%'
}

const noAvatarStyle = {
  maxHeight: '48px'
}

const HeaderComp = styled.header`
  align-items: center;
  display: flex;
  margin-bottom: 8px;
  padding: 0.8em;
`
const AvatarDiv = styled.div`
  z-index: 1000;
  position: absolute;
  width: 26px;
  height: 28px;
  right: 97px;
  top: 9px;
`

const SubTitle = styled.span`
  font-size: 0.5em;

  @media(max-width: 699px) {
    display: none;
  }
`

const MenuLink = styled(Link)`
  font-size: 1.2em;
  display: block;
  margin-bottom: 0, 1.2em;
  padding: 0.8em 0;
  border-bottom: 1px solid #fff;
  text-decoration: none;

  &:visited {
    color: white;
  }
`
class Header extends Component {
  constructor (props) {
    super(props)
    this.state = {
      menuOpen: false
    }
    this.handleMenuClick = this.handleMenuClick.bind(this)
  }

  handleMenuClick (e) {
    this.setState({ menuOpen: false })
  }

  render () {
    return (
      <AuthConsumer>
        {({ user, anonymous, userScore, logout }) => (
          <HeaderComp className='App-header'>
            <Link to='/'><img style={noAvatarStyle} src={Logo} className='App-logo' alt='logo' /></Link>
            <NavBarTitle>
              Dreamerbase
              <SubTitle>: a react/firebase starter</SubTitle>
            </NavBarTitle>
            { user && !anonymous && <AvatarDiv className='user-profile'>
              <img style={avatarStyle} className='avatar' alt='my dashboard' src={user.photoURL || Logo} />
            </AvatarDiv>
            }
            <StyledMenu isOpen={this.state.menuOpen} right styles={styles}>
              <MenuLink onClick={this.handleMenuClick} id='home' className='menu-item' to='/'>Home</MenuLink>
              <MenuLink onClick={this.handleMenuClick} id='about' className='menu-item' to='/about'>About</MenuLink>
              { user && !anonymous && <MenuLink onClick={e => { this.handleMenuClick(e); logout() }} id='logout' to='/' className='menu-item'>Logout</MenuLink> }
              { (!user || anonymous) && <MenuLink onClick={this.handleMenuClick} id='emailLogin' className='menu-item' to='/login'>Login</MenuLink> }
            </StyledMenu>
          </HeaderComp>
        )}
      </AuthConsumer>
    )
  }
}

export default Header
