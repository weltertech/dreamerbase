import React from 'react'
import { Link } from 'react-router-dom'
import styled from 'styled-components'

const Footer = styled.footer`
  display: flex;
  flex-wrap: wrap;
  justify-content: space-around;
  padding: 1em;
  font-size: 1.2em;
  font-family: "Indie Flower", cursive;
  strong {
    font-weight: 800;
  }
  div {
    padding-bottom: 15px;
    a {
      text-decoration: none;
    }
  }
`

export default () => (
  <Footer>
    <div><Link to='/terms'>Terms of Service</Link></div>
    <div><Link to='/privacy'>Privacy Policy</Link></div>
    <div><strong>Copyright © 2019 COMPANY_NAME</strong></div>
  </Footer>
)
