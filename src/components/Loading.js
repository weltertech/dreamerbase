import React from 'react'
import styled from 'styled-components'
import Container from './Container'

const LoadinvSvg = styled.svg`
  height: 100vh;
  width: 100%;
`

const LoadingContainer = styled(Container)`
  align-items: center;
  box-sizing: border-box;
  flex-direction: column;
  display: flex;
  height: 100vh;
  justify-content: center;
`

const UserProfile = () => (
  <LoadingContainer>
    <LoadinvSvg className='lds-blocks' xmlns='http://www.w3.org/2000/svg' viewBox='0 0 100 100'
      preserveAspectRatio='xMidYMid'>
      <rect x='19' y='19' width='20' height='20' fill='#222'>
        <animate attributeName='fill' values='#50b557;#222222;#222222' keyTimes='0;0.125;1'
          dur='1.7s' repeatCount='indefinite' begin='0s' calcMode='discrete' />
      </rect>
      <rect x='40' y='19' width='20' height='20' fill='#222'>
        <animate attributeName='fill' values='#50b557;#222222;#222222' keyTimes='0;0.125;1'
          dur='1.7s' repeatCount='indefinite' begin='0.2125s' calcMode='discrete'
        />
      </rect>
      <rect x='61' y='19' width='20' height='20' fill='#222'>
        <animate attributeName='fill' values='#50b557;#222222;#222222' keyTimes='0;0.125;1'
          dur='1.7s' repeatCount='indefinite' begin='0.425s' calcMode='discrete'
        />
      </rect>
      <rect x='19' y='40' width='20' height='20' fill='#222'>
        <animate attributeName='fill' values='#50b557;#222222;#222222' keyTimes='0;0.125;1'
          dur='1.7s' repeatCount='indefinite' begin='1.4875s' calcMode='discrete'
        />
      </rect>
      <rect x='61' y='40' width='20' height='20' fill='#222'>
        <animate attributeName='fill' values='#50b557;#222222;#222222' keyTimes='0;0.125;1'
          dur='1.7s' repeatCount='indefinite' begin='0.6375s' calcMode='discrete'
        />
      </rect>
      <rect x='19' y='61' width='20' height='20' fill='#222'>
        <animate attributeName='fill' values='#50b557;#222222;#222222' keyTimes='0;0.125;1'
          dur='1.7s' repeatCount='indefinite' begin='1.275s' calcMode='discrete'
        />
      </rect>
      <rect x='40' y='61' width='20' height='20' fill='#222'>
        <animate attributeName='fill' values='#50b557;#222222;#222222' keyTimes='0;0.125;1'
          dur='1.7s' repeatCount='indefinite' begin='1.0625s' calcMode='discrete'
        />
      </rect>
      <rect x='61' y='61' width='20' height='20' fill='#222'>
        <animate attributeName='fill' values='#50b557;#222222;#222222' keyTimes='0;0.125;1'
          dur='1.7s' repeatCount='indefinite' begin='0.85s' calcMode='discrete' />
      </rect>
    </LoadinvSvg>
  </LoadingContainer>
)

export default UserProfile
