export const primary = 'palevioletred'
export const secondary = '#68BDEA'
export const tertiary = '#50B557'
export const info = '#ACD7EC'
export const error = '#EF2D56'

export const penny = '#373a47'
