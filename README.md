# Dreamerbase - Firebase/React Starter

To start, add your firebase info to `.env` and `.firebaserc`. Then run...

For local development...

```
firebase deploy --only database
npm start
```

To deploy to firebase...
```
firebase deploy
npm run deploy-ui
```

We have a two-step deploy process in order to have a dynamic html file served up by an express serverless function.
