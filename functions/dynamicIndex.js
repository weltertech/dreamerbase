/**
 * Express server for generating unique html for pages
 */
const express = require('express')
const exphbs = require('express-handlebars')
const cors = require('cors')

const app = express()
app.set('view engine', 'handlebars')
app.engine('handlebars', exphbs({ defaultLayout: 'main' }))

// Ensure request is actually coming via firebase?
// app.use(firebaseUser.validateFirebaseIdToken)

// Automatically allow cross-origin requests
app.use(cors({ origin: true }))

// build multiple CRUD interfaces:
app.get('/', (req, res) => {
  return res.render('home', {
    url: 'https://yoursite.com',
    title: 'Dreamerbase',
    description: 'A React/Firebase Starter'
  })
})

app.get('/index.html', (req, res) => {
  return res.render('home', {
    url: 'https://yoursite.com',
    title: 'Dreamerbase',
    description: 'A React/Firebase Starter'
  })
})

exports.server = (database, functions) => {
  return functions.https.onRequest(app)
}
