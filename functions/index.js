const functions = require('firebase-functions')
const admin = require('firebase-admin')

admin.initializeApp()
const db = admin.database()

const dynamicIndexServer = require('./dynamicIndex')

// Dynamic Index.html express server
exports.dynamicIndex = dynamicIndexServer.server(db, functions)
